package ru.rvision.graduate.api_tests;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import ru.rvision.graduate.api_tests.model.Pet;

import java.io.File;
import java.util.List;
import java.util.Map;

import static ru.rvision.graduate.api_tests.CreateJSONFile.random;

public class BaseTest {

    static String baseUrl = "https://petstore.swagger.io/v2";
    static Faker faker = new Faker();

    File file = new File(String.format("src/test/resources/images/Image%d.jpg",
            random.nextInt(1, 5)));

    File fileEmpty = new File("");

    static Map<String, List<String>> queryParamsForFindByStatus = Map.of(
            "status", List.of("available", "pending", "sold")
    );

    public static Pet returnObjectPet(String petID){
        return RestAssured
                .given()
                .spec(baseGetNoLogSpec())
                .pathParam("petId", petID)
                .when()
                .get("/pet/{petId}")
                .then()
                .spec(responseBaseNoLogSpec())
                .extract()
                .jsonPath()
                .getObject("$", Pet.class);
    }

    public static RequestSpecification baseGetSpec() {
        return new RequestSpecBuilder()
                .setBaseUri(baseUrl)
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();
    }
    public static RequestSpecification baseGetNoLogSpec() {
        return new RequestSpecBuilder()
                .setBaseUri(baseUrl)
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .build();
    }

    public static RequestSpecification baseSendImageSpec(){
        return new RequestSpecBuilder()
                .setBaseUri(baseUrl)
                .setAccept(ContentType.JSON)
                .setContentType("multipart/form-data")
                .log(LogDetail.ALL)
                .build();
    }

    public static ResponseSpecification responseBaseSpec() {
        return new ResponseSpecBuilder()
                .log(LogDetail.ALL)
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200)
                .build();
    }

    public static ResponseSpecification responseBaseNoLogSpec() {
        return new ResponseSpecBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200)
                .build();
    }

}
