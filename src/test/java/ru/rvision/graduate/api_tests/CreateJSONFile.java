package ru.rvision.graduate.api_tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import ru.rvision.graduate.api_tests.model.Category;
import ru.rvision.graduate.api_tests.model.Pet;
import ru.rvision.graduate.api_tests.model.Store;
import ru.rvision.graduate.api_tests.model.Tag;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static ru.rvision.graduate.api_tests.BaseTest.queryParamsForFindByStatus;

public class CreateJSONFile {

    static ObjectMapper objectMapper = new ObjectMapper();
    static Random random = new Random();
    static Faker faker = new Faker();


    public static String pojoToJsonStringForPet() {
        try {
            Pet pet = new Pet(
                    generatorIdForPet(18),
                    new Category(
                            generatorIdForPet(8),
                            "Cat"
                    ),
                    faker.cat().name(),
                    new ArrayList<>(List.of(faker.avatar().image())),
                    new ArrayList<>(List.of(new Tag(
                            generatorIdForPet(5),
                            faker.cat().breed()))),
                    queryParamsForFindByStatus
                            .get("status")
                            .get(random
                                    .nextInt(3)));
            return objectMapper.writeValueAsString(pet);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String pojoToJsonStringForPet(Long customId) {
        try {
            Pet pet = new Pet(
                    customId,
                    new Category(
                            generatorIdForPet(8),
                            faker.animal().name()
                    ),
                    faker.cat().name(),
                    new ArrayList<>(List.of(faker.avatar().image())),
                    new ArrayList<>(List.of(new Tag(
                            generatorIdForPet(5),
                            faker.cat().breed()))),
                    queryParamsForFindByStatus
                            .get("status")
                            .get(random
                                    .nextInt(3)));
            return objectMapper.writeValueAsString(pet);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String pojoToJsonStringForStore(Long petId) {
        try {
            Store store = new Store(
                    random.nextInt(1,11),
                    petId,
                    random.nextInt(1000),
                    "2024-01-15T12:45:51.317+0000",
                    queryParamsForFindByStatus
                            .get("status")
                            .get(random
                                    .nextInt(3)),
                    true
                    );
            return objectMapper.writeValueAsString(store);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static Long generatorIdForPet(int lengthId) {
        StringBuilder number = new StringBuilder();
        for (int i = 0; i < lengthId; i++) {
            number.append(random.nextInt(1,10));
        }
        return Long.parseLong(number.toString());
    }
}
