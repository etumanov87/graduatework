package ru.rvision.graduate.api_tests;

import io.qameta.allure.Story;
import io.qameta.allure.testng.Tag;
import io.restassured.RestAssured;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.rvision.graduate.api_tests.model.Pet;

import java.io.FileNotFoundException;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static ru.rvision.graduate.api_tests.CreateJSONFile.*;


@Story("Проверка раздела API '/pet'")
public class TestForPet extends BaseTest {

    @Tag("findByStatusPet")
    @Test(description = "Проверка, что при выборке по категории Cat возвращаются только Cat")
    public void checkSampleByCategoryCat() {
        boolean resultSampleByCategoryCat = RestAssured
                .given()
                .spec(baseGetSpec())
                .queryParams(queryParamsForFindByStatus)
                .when()
                .get("/pet/findByStatus")
                .then()
                .spec(responseBaseSpec())
                .extract()
                .body()
                .jsonPath()
                .getList("$", Pet.class)
                .stream()
                .filter(pet -> pet.category != null)
                .filter(pet -> pet.category.name.equalsIgnoreCase("cat"))
                .allMatch(pet -> pet.category.name.equalsIgnoreCase("cat"));
        Assert.assertTrue(resultSampleByCategoryCat);
    }

    @Tag("addNewPet")
    @Test(description = "Проверка создание нового питомца")
    public void checkCreateNewPet() {
        RestAssured
                .given()
                .spec(baseGetSpec())
                .body(pojoToJsonStringForPet())
                .when()
                .post("/pet")
                .then()
                .spec(responseBaseSpec())
                .extract()
                .response();
    }

    @Tag("getInfoPetById")
    @Test(description = "Проверка попытки получения информации о несуществующем питомце")
    public void checkGetNonExistentPetById() {
        RestAssured
                .given()
                .spec(baseGetSpec())
                .when()
                .get("/pet/999")
                .then()
                .statusCode(404);
    }

    @DataProvider(name = "petData")
    public Object[][] dataProviderForGetPetById() {
        Pet newPet = RestAssured
                .given()
                .spec(baseGetNoLogSpec())
                .body(pojoToJsonStringForPet())
                .when()
                .post("/pet")
                .then()
                .spec(responseBaseNoLogSpec())
                .extract()
                .body()
                .jsonPath()
                .getObject("$", Pet.class);

        return new Object[][]{
                {newPet.id, newPet.name},
        };
    }

    @Tag("getInfoPetById")
    @Test(description = "Получения информации о конкретном питомце",
            dataProvider = "petData")
    public void testGetPetById(Long petId, String name) {
        RestAssured
                .given()
                .spec(baseGetSpec())
                .pathParam("petId", petId.toString())
                .when()
                .get("/pet/{petId}")
                .then()
                .spec(responseBaseSpec())
                .body("id", equalTo(petId))
                .body("name", equalTo(name));
    }

    @DataProvider(name = "randomIdData")
    public Object[][] dataProviderForRandomId() {
        List<Long> petList = RestAssured
                .given()
                .spec(baseGetNoLogSpec())
                .queryParams(queryParamsForFindByStatus)
                .when()
                .get("/pet/findByStatus")
                .then()
                .spec(responseBaseNoLogSpec())
                .extract()
                .body()
                .jsonPath()
                .getList("$", Pet.class)
                .stream()
                .map(pet -> pet.id)
                .toList();

        String randomId = petList.get(random.nextInt(0, petList.size() - 1)).toString();

        return new Object[][]{
                {randomId}
        };
    }

    @Tag("changeNameAndStatusPet")
    @Test(description = "Изменить имя у случайного питомца",
            dataProvider = "randomIdData")
    public void changeNameRandomPetById(String randomId) {
        RestAssured
                .given()
                .spec(baseGetSpec())
                .contentType("application/x-www-form-urlencoded")
                .formParam("name", faker.cat().name())
                .pathParam("petId", randomId)
                .when()
                .post("/pet/{petId}")
                .then()
                .spec(responseBaseSpec())
                .body("message", equalTo(randomId));
    }

    @Tag("changeNameAndStatusPet")
    @Test(description = "Проверить изменения статус у случайного питомца",
            dataProvider = "randomIdData")
    public void checkChangeStatusRandomPetById(String randomId) {
        String expectedStatus = returnObjectPet(randomId)
                .status;

        RestAssured
                .given()
                .spec(baseGetSpec())
                .contentType("application/x-www-form-urlencoded")
                .formParam("status", faker.job().keySkills())
                .pathParam("petId", randomId)
                .when()
                .post("/pet/{petId}")
                .then()
                .spec(responseBaseSpec())
                .body("message", equalTo(randomId));

        String actualStatus = returnObjectPet(randomId)
                .status;

        Assert.assertNotEquals(actualStatus,expectedStatus, "Status not change!");
    }

    @Tag("changeNameAndStatusPet")
    @Test(description = "Проверить изменения c пустыми значениями названия и статуса у случайного питомца",
            dataProvider = "randomIdData")
    public void checkChangeNullNameAndStatusRandomPetById(String randomId) {
        RestAssured
                .given()
                .spec(baseGetSpec())
                .contentType("application/x-www-form-urlencoded")
                .pathParam("petId", randomId)
                .when()
                .post("/pet/{petId}")
                .then()
                .spec(responseBaseSpec())
                .body("message", equalTo(randomId));
    }

    @Tag("deletePet")
    @Test(description = "Проверка попытки удалить несуществующего питомца")
    public void checkDeleteNonExistentPetById() {
        RestAssured
                .given()
                .spec(baseGetSpec())
                .when()
                .delete("/pet/999")
                .then()
                .statusCode(404);
    }

    @Tag("deletePet")
    @Test(description = "Проверка удаления случайного питомца",
            dataProvider = "randomIdData")
    public void checkDeleteRandomPetById(String randomId) {
        RestAssured
                .given()
                .spec(baseGetSpec())
                .pathParam("petId", randomId)
                .when()
                .delete("/pet/{petId}")
                .then()
                .spec(responseBaseSpec())
                .body("message", equalTo(randomId));
    }

    @Tag("updatePet")
    @Test(description = "Изменить данные для случайного питомца",
            dataProvider = "randomIdData")
    public void changeDataRandomPetById(String randomId) {
        RestAssured
                .given()
                .spec(baseGetSpec())
                .body(pojoToJsonStringForPet(Long.parseLong(randomId)))
                .when()
                .put("/pet")
                .then()
                .spec(responseBaseSpec())
                .extract()
                .response();
    }

    @Tag("updatePet")
    @Test(description = "Проверка попытки изменить данные для случайного питомца")
    public void checkChangeDataRandomPetById() {
        RestAssured
                .given()
                .spec(baseGetSpec())
                .body("")
                .when()
                .put("/pet")
                .then()
                .statusCode(405);
    }

    @Tag("addImage")
    @Test(description = "Добавить картинку к случайному питомцу",
            dataProvider = "randomIdData")
    public void addImagePetById(String randomId) {
        RestAssured
                .given()
                .spec(baseSendImageSpec())
                .pathParam("petId", randomId)
                .formParam("additionalMetadata","test")
                .multiPart("file", file, "image/jpeg")
                .when()
                .post("/pet/{petId}/uploadImage")
                .then()
                .spec(responseBaseSpec())
                .extract()
                .response();
    }

    @Tag("addImage")
    @Test(description = "Проверить попытку отправки не корректного запроса к случайному питомцу",
            dataProvider = "randomIdData")
    public void checkIncorrectRequestWasSentPetById(String randomId) {
            RestAssured
                    .given()
                    .spec(baseSendImageSpec())
                    .formParam("additionalMetadata","test")
                    .pathParam("petId", randomId)
                    .multiPart("", "")
                    .when()
                    .post("/pet/{petId}/uploadImage")
                    .then()
                    .statusCode(500);
    }



}

