package ru.rvision.graduate.api_tests;

import io.qameta.allure.Story;
import io.qameta.allure.testng.Tag;
import io.restassured.RestAssured;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.rvision.graduate.api_tests.model.Pet;
import ru.rvision.graduate.api_tests.model.Store;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static ru.rvision.graduate.api_tests.CreateJSONFile.*;


@Story("Проверка раздела API '/store'")
public class TestForStore extends BaseTest {


    @DataProvider(name = "petData")
    public Object[][] dataProviderForNewOrderByPetId() {
        Pet newPet = RestAssured
                .given()
                .spec(baseGetNoLogSpec())
                .body(pojoToJsonStringForPet())
                .when()
                .post("/pet")
                .then()
                .spec(responseBaseNoLogSpec())
                .extract()
                .body()
                .jsonPath()
                .getObject("$", Pet.class);

        return new Object[][]{
                {newPet.id},
        };
    }

    @Tag("addOrder")
    @Test(description = "Добавить новый заказ на приобретение питомца",
            dataProvider = "petData")
    public void addNewOrderByPetId(Long petId) {
        RestAssured
                .given()
                .spec(baseGetSpec())
                .body(pojoToJsonStringForStore(petId))
                .when()
                .post("/store/order")
                .then()
                .spec(responseBaseSpec())
                .extract()
                .response();
    }

    @Tag("addOrder")
    @Test(description = "Проверка попытки создания нового заказа с несуществующими данными")
    public void checkAddNewOrderNonExistent() {
        RestAssured
                .given()
                .spec(baseGetSpec())
                .body("")
                .when()
                .post("/store/order")
                .then()
                .statusCode(400);
    }
    @DataProvider(name = "existingOrderId")
    public Object[][] dataProviderFor() {
        Store orderId = RestAssured
                .given()
                .spec(baseGetSpec())
                .body(pojoToJsonStringForStore(generatorIdForPet(18)))
                .when()
                .post("/store/order")
                .then()
                .spec(responseBaseSpec())
                .extract()
                .jsonPath()
                .getObject("$", Store.class);;

        return new Object[][]{
                {orderId.id},
        };
    }

    @Tag("getInfoOrder")
    @Test(description = "Получение информации о существующем заказе по его Id",
            dataProvider = "existingOrderId")
    public void getInfoByOrderById(int orderId) {
        RestAssured
                .given()
                .spec(baseGetSpec())
                .pathParam("orderId", orderId)
                .when()
                .get("/store/order/{orderId}")
                .then()
                .spec(responseBaseSpec())
                .extract()
                .response();
    }

    @Tag("getInfoOrder")
    @Test(description = "Проверка, что для заказа с невалидным Id возвращается исключение")
    public void checkOrderByIdInvalid() {
        RestAssured
                .given()
                .spec(baseGetSpec())
                .pathParam("orderId", random.nextInt(150, 999))
                .when()
                .get("/store/order/{orderId}")
                .then()
                .statusCode(404)
                .body("message", equalTo("Order not found"));
    }


    @Tag("deleteOrder")
    @Test(description = "Удалить заказ по его Id",
            dataProvider = "existingOrderId")
    public void deleteOrderById(int orderId) {
        RestAssured
                .given()
                .spec(baseGetSpec())
                .pathParam("orderId", orderId)
                .when()
                .delete("/store/order/{orderId}")
                .then()
                .spec(responseBaseSpec())
                .extract()
                .response();
    }

    @Tag("deleteOrder")
    @Test(description = "Проверить что невозможно удалить не существующий заказ")
    public void checkDeleteOrderNonExistent() {
        RestAssured
                .given()
                .spec(baseGetSpec())
                .pathParam("orderId", random.nextInt(1000, 2000))
                .when()
                .delete("/store/order/{orderId}")
                .then()
                .statusCode(404)
                .body("code", equalTo(404));
    }

    @Tag("getListStatusCodes")
    @Test(description = "Проверить что в списке нет статусов с пустыми значений")
    public void checkStatusNotEmptyValues() {
        RestAssured
                .given()
                .spec(baseGetSpec())
                .when()
                .get("/store/inventory")
                .then()
                .spec(responseBaseSpec())
                .body(queryParamsForFindByStatus
                        .get("status")
                        .get(random.nextInt(0, 3)), notNullValue());
    }
}
