package ru.rvision.graduate.api_tests.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Store {
    public int id;
    public long petId;
    public int quantity;
    public String shipDate;
    public String status;
    public boolean complete;
}