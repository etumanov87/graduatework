package ru.rvision.graduate.ui_tests.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.$x;

public class DashboardPage {

    ElementsCollection getLegendKeyUnassigned = $$x("//span[@title='Unassigned']");
    SelenideElement nameLegendKeyUnassignedThrough = $x("//span[@style='text-decoration: line-through;']");
    String menuItemXpathTemplate = "//span[text()='%s']";

    @Step("Выбрать раздел {sectionName} главного меню")
    public void selectSectionMainMenu(String sectionName) {
        $x(String.format(menuItemXpathTemplate, sectionName)).click();
    }

    @Step("Проверить что легенда 'Unassigned' существует")
    public DashboardPage checkLegendKeyUnassigned() {
        getLegendKeyUnassigned.get(0).shouldBe(visible);
        return this;
    }

    @Step("Найти и выбрать легенду № {number} с название 'Unassigned'")
    public DashboardPage findAndClickAllLegendKeyUnassigned(int number) {
        getLegendKeyUnassigned.get(number - 1).shouldBe(visible).click();
        return this;
    }

    @Step("Проверить что у легенды 'Unassigned' название стало зачеркнуты")
    public DashboardPage checkAllNameLegendKeyUnassignedThrough() {
        nameLegendKeyUnassignedThrough.shouldBe(visible);
        return this;
    }
}
