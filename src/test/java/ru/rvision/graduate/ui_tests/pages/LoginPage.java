package ru.rvision.graduate.ui_tests.pages;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$x;

public class LoginPage {
    SelenideElement userNameField = $x("//input[@name='username']");
    SelenideElement passwordField = $x("//input[@name='password']");
    SelenideElement loginButton = $x("//button[text()=' Login ']");

    @Step("Открыть страницу Авторизации на сайте 'https://opensource-demo.orangehrmlive.com/'")
    public LoginPage open() {
        Configuration.remote = "http://51.250.123.179:4444/wd/hub";
        Selenide.open("https://opensource-demo.orangehrmlive.com/");
        return this;
    }

    @Step("Ввести логин {login}")
    public LoginPage setUserName(String login) {
        userNameField.setValue(login);
        return this;
    }
    @Step("Ввести пароль {password}")
    public LoginPage setUserPassword(String password) {
        passwordField.setValue(password);
        return this;
    }

    @Step("Нажать на кнопку Login")
    public void clickLoginButton(){
        loginButton.click();
    }
}
