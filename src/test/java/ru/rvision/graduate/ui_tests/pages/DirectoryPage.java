package ru.rvision.graduate.ui_tests.pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class DirectoryPage {
    SelenideElement findCardUser = $x("//div[contains(@class, 'oxd-sheet')]");
    SelenideElement findCardUserRightMenu = $x("//div[@class='orangehrm-corporate-directory-sidebar']");

    @Step("Нажат по карточке пользователя")
    public DirectoryPage clickForFirstUser() {
        findCardUser.click();
        return this;
    }

    @Step("Проверить что появилось право меню с информацией о пользователе")
    public DirectoryPage checkInfoForUser() {
        findCardUserRightMenu.shouldBe(visible);
        return this;
    }
}
