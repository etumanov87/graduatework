package ru.rvision.graduate.ui_tests.pages;

import com.github.javafaker.Faker;
import ru.rvision.graduate.ui_tests.pages.DashboardPage;
import ru.rvision.graduate.ui_tests.pages.LoginPage;
import ru.rvision.graduate.ui_tests.pages.PIMPage;

import java.util.Random;

public interface PageObjectSupplierLitres {
    Faker faker = new Faker();
    Random random = new Random();
    default LoginPage loginPage() {
        return new LoginPage();
    }

    default DashboardPage dashboardPage() {return new DashboardPage();}

    default PIMPage pimPage() {return new PIMPage();}

    default ClaimPage claimPage() {return new ClaimPage();}

    default DirectoryPage directoryPage() {return new DirectoryPage();}

    default RecruitmentPage recruitmentPage() {return new RecruitmentPage();}
}
