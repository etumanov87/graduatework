package ru.rvision.graduate.ui_tests.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import java.time.Duration;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class RecruitmentPage {

    SelenideElement buttonAdd = $x("//div[@class='orangehrm-header-container']//button");
    SelenideElement inputFirstName = $x("//input[@name='firstName']");
    SelenideElement inputLastName = $x("//input[@name='lastName']");
    SelenideElement inputEmail = $x("//label[text()='Email']/parent::div/following-sibling::div//input");
    SelenideElement buttonSave = $x("//button[@type='submit']");
    SelenideElement buttonCancel = $x("//button[text()=' Cancel ']");
    SelenideElement cardCandidateDetails = $x("//h6[text()='Application Stage']");
    SelenideElement buttonVacancies = $x("//li/a[text()='Vacancies']");
    SelenideElement inputVacancyName = $x("//label[text()='Vacancy Name']/parent::div/following-sibling::div//input");
    SelenideElement inputHiringManager = $x("//label[text()='Hiring Manager']/parent::div/following-sibling::div//input");
    SelenideElement selectJobTitle = $x("//label[text()='Job Title']/parent::div/following-sibling::div//div[@class='oxd-select-text-input']");
    SelenideElement selectVacancy = $x("//label[text()='Vacancy']/parent::div/following-sibling::div//div[@class='oxd-select-text-input']");
    SelenideElement buttonSearch = $x("//div[@class='oxd-form-actions']/button[@type='submit']");
    SelenideElement checkboxFirstSelect = $x("//div[@class='oxd-table-body']//input[@value='0']/following-sibling::span/i");
    SelenideElement buttonDelete = $x("//button/i[contains(@class, 'bi-trash-fill')]");
    SelenideElement buttonConfirmDelete = $x("//div[@class='orangehrm-modal-footer']/button/i");
    ElementsCollection fieldRequired = $$x("//span[text()='Required']");
    SelenideElement fieldRequiredFirstName = $x("//input[@name='firstName']/parent::div/following-sibling::span");
    SelenideElement fieldRequiredLastName = $x("//input[@name='lastName']/parent::div/following-sibling::span");
    SelenideElement fieldRequiredEmail = $x("//label[text()='Email']/parent::div/following-sibling::span");
    SelenideElement fieldRequiredVacancyName = $x("//label[text()='Vacancy Name']/parent::div/following-sibling::span");
    SelenideElement fieldRequiredHiringManager = $x("//label[text()='Hiring Manager']/parent::div/following-sibling::span");
    SelenideElement fieldRequiredJobTitle = $x("//label[text()='Job Title']/parent::div/following-sibling::span");


    @Step("Нажать на кнопку Add")
    public RecruitmentPage clickButtonAdd() {
        buttonAdd.click();
        return this;
    }

    @Step("Нажать на кнопку Save")
    public RecruitmentPage clickButtonSave() {
        buttonSave.click();
        return this;
    }
    @Step("Нажать на кнопку Cancel")
    public RecruitmentPage clickButtonCancel() {
        buttonCancel.click();
        return this;
    }

    @Step("Ввести имя {firstName} кандидата")
    public RecruitmentPage setFirstName(String firstName) {
        inputFirstName.setValue(firstName);
        return this;
    }

    @Step("Ввести фамилию {lastName} кандидата")
    public RecruitmentPage setLastName(String lastName) {
        inputLastName.setValue(lastName);
        return this;
    }

    @Step("Ввести электронный адрес кандидата")
    public RecruitmentPage setEmail(String mail) {
        inputEmail.setValue(mail);
        return this;
    }

    @Step("Проверить что добавился новый кандидат")
    public RecruitmentPage checkCandidateHasAdded() {
        cardCandidateDetails.shouldBe(visible);
        return this;
    }

    @Step("Нажать на кнопку Vacancies")
    public RecruitmentPage clickButtonVacancies() {
        buttonVacancies.click();
        return this;
    }

    @Step("Ввести название {vacancyName} вакансии")
    public RecruitmentPage setVacancyName(String vacancyName) {
        inputVacancyName.setValue(vacancyName);
        return this;
    }

    @Step("Выбрать значение для поля Job Title")
    public RecruitmentPage selectJobTitle() {
        selectJobTitle.click();
        $x("//span[text()='Content Specialist']").click();
        return this;
    }

    @Step("Выбрать значения для поля Hiring Manager")
    public RecruitmentPage setHiringManager() {
        inputHiringManager.sendKeys("p");
        $x("//span[text()='Peter Mac Anderson']").click();
        return this;
    }

    @Step("Проверить что новая вакансия {vacancyName} добавилась")
    public RecruitmentPage checkVacanciesHasAdded(String vacancyName) {
        selectVacancy.click();
        $x(String.format("//span[text()='%s']", vacancyName)).click();
        buttonSearch.click();
        checkboxFirstSelect.click();
        buttonDelete.shouldBe(visible);
        return this;
    }

    @Step("Выбрать первый чек бокс в списке")
    public RecruitmentPage selectCheckboxFirst() {
        checkboxFirstSelect.click();
        return this;
    }

    @Step("Нажать на кнопку Delete и подтвердить удаление")
    public RecruitmentPage clickButtonDelete() {
        buttonDelete.click();
        buttonConfirmDelete.click();
        return this;
    }
    @Step("Проверить обязательных полей при добавлении кандидата")
    public RecruitmentPage checkRequiredFieldCandidate() {
        fieldRequiredFirstName.shouldBe(visible);
        fieldRequiredLastName.shouldBe(visible);
        fieldRequiredEmail.shouldBe(visible);
        return this;
    }
    @Step("Проверить обязательных полей при добавлении вакансии")
    public RecruitmentPage checkRequiredFieldVacancies() {
        fieldRequiredVacancyName.shouldBe(visible);
        fieldRequiredHiringManager.shouldBe(visible);
        fieldRequiredJobTitle.shouldBe(visible);
        return this;
    }
}
