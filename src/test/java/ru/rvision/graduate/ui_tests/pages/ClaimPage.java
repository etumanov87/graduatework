package ru.rvision.graduate.ui_tests.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import java.time.Duration;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class ClaimPage {
    SelenideElement buttonAssignClaim = $x("//div[@class='orangehrm-header-container']//button");
    SelenideElement buttonAdd = $x("//div[@class='orangehrm-header-container']//button");
    SelenideElement buttonSave = $x("//div[@class='oxd-form-actions']/button[@type='submit']");
    SelenideElement inputEmployeeName = $x("//label[text()='Employee Name']/parent::div/following-sibling::div//input");
    SelenideElement inputEvent = $x("//label[text()='Event']/parent::div/following-sibling::div//i");
    SelenideElement inputCurrency = $x("//label[text()='Currency']/parent::div/following-sibling::div//i");
    SelenideElement inputEventName = $x("//label[text()='Event Name']/parent::div/following-sibling::div//input");
    SelenideElement buttonCreate = $x("//div[@class='oxd-form-actions']/button[@type='submit']");
    SelenideElement buttonSubmit = $x("//div[@class='orangehrm-action-button-container']//button[text()=' Submit ']");
    SelenideElement buttonEmployeeClaims = $x("//li/a[text()='Employee Claims']");
    SelenideElement buttonSearch = $x("//div[@class='oxd-form-actions']/button[@type='submit']");
    SelenideElement buttonViewDetails = $x("//div[@class='oxd-table-cell-actions']//button[text() = ' View Details ']");
    SelenideElement buttonConfiguration = $x("//li/span[text()='Configuration ']/i");
    SelenideElement disableInputEmployee = $x("//label[text()='Employee']/parent::div/following-sibling::div//input");
    SelenideElement checkSectionEvent = $x("//h5[text()='Events']");
    SelenideElement checkboxFirstSelect = $x("//div[@class='oxd-table-body']//input[@value='0']/following-sibling::span/i");
    SelenideElement buttonDelete = $x("//button/i[contains(@class, 'bi-trash-fill')]");
    SelenideElement buttonConfirmDelete = $x("//div[@class='orangehrm-modal-footer']/button/i");
    ElementsCollection fieldRequired = $$x("//span[text()='Required']");


    @Step("Нажать кнопку Assign Claim")
    public ClaimPage clickButtonAssignClaim() {
        buttonAssignClaim.click();
        return this;

    }

    @Step("Выбрать в выпадающем списке Сотрудника {firstName} {lastName}")
    public ClaimPage setEmployeeName(String firstName, String lastName) {
        inputEmployeeName.sendKeys(firstName);
        $x(String.format("//span[text()='%s  %s']", firstName, lastName)).click();
        return this;
    }

    @Step("Выбрать в выпадающем списке Событие")
    public ClaimPage selectEvent() {
        inputEvent.click();
        $x("//span[text()='Accommodation']").click();
        return this;
    }

    @Step("Выбрать в выпадающем списке Валюту")
    public ClaimPage selectCurrency() {
        inputCurrency.click();
        $x("//span[text()='Botswana Pula']").click();
        return this;
    }

    @Step("Нажать кнопку Create")
    public ClaimPage clickButtonCreate() {
        buttonCreate.click();
        return this;
    }

    @Step("Нажать кнопку Submit")
    public ClaimPage clickButtonSubmit() {
        buttonSubmit.shouldBe(visible, Duration.ofMillis(5000)).click();
        return this;
    }

    @Step("Нажать кнопку Employee Claims")
    public ClaimPage clickButtonEmployeeClaims() {
        buttonEmployeeClaims.click();
        return this;
    }

    @Step("Нажать кнопку Search")
    public ClaimPage clickButtonSearch() {
        buttonSearch.click();
        return this;
    }

    @Step("Проверить что Claim добавился")
    public ClaimPage checkClaimHasAdded() {
        buttonViewDetails.shouldBe(visible).click();
        disableInputEmployee.shouldBe(disabled);
        return this;
    }

    @Step("Нажать кнопку Add")
    public ClaimPage clickButtonAdd() {
        buttonAdd.click();
        return this;
    }

    @Step("Нажать кнопку Save")
    public ClaimPage clickButtonSave() {
        buttonSave.click();
        return this;
    }

    @Step("Нажать кнопку Delete")
    public ClaimPage clickButtonDelete() {
        buttonDelete.click();
        return this;
    }

    @Step("Нажать кнопку подтвердить удаление")
    public ClaimPage clickConfirmDelete() {
        buttonConfirmDelete.click();
        return this;
    }

    @Step("Нажать на кнопку Configuration и выбрать раздел События")
    public ClaimPage clickSectionEvent() {
        buttonConfiguration.click();
        $x("//a[text()='Events']").click();
        return this;
    }

    @Step("Ввести название События")
    public ClaimPage setNameEvent(String nameEvent) {
        inputEventName.setValue(nameEvent);
        return this;
    }

    @Step("Проверить что страница находится в разделе События")
    public ClaimPage checkEventHasAdded() {
        checkSectionEvent.shouldBe(visible);
        return this;
    }

    @Step("Ввести название события для поиска")
    public ClaimPage setNameEventForSearch(String nameEvent) {
        inputEventName.sendKeys(nameEvent);
        $x(String.format("//span[text()='%s']", nameEvent)).click();
        return this;
    }
    @Step("Выбрать первый чек-бокс из списка")
    public ClaimPage selectFirstCheckbox() {
        checkboxFirstSelect.click();
        return this;
    }
}
