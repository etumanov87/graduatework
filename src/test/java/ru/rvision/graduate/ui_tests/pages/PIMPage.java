package ru.rvision.graduate.ui_tests.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class PIMPage {

    SelenideElement buttonAdd = $x("//div[@class='orangehrm-header-container']//button");
    SelenideElement inputFirstName = $x("//input[@name='firstName']");
    SelenideElement inputEmployeeName = $x("//label[text()='Employee Name']/parent::div/following-sibling::div//input");
    SelenideElement inputLastName = $x("//input[@name='lastName']");
    SelenideElement inputId = $x("//label[text()='Employee Id']/parent::div/following-sibling::div/input");
    SelenideElement inputReportName = $x("//label[text()='Report Name']/parent::div/following-sibling::div//input");
    SelenideElement buttonSave = $x("//button[@type='submit']");
    SelenideElement cardPersonalDetails = $x("//h6[text()='Personal Details']");
    SelenideElement selectAllEmployee = $x("//div[@class='oxd-table-header']//i");
    SelenideElement buttonDeleteEmployee = $x("//button/i[contains(@class, 'bi-trash-fill')]");
    SelenideElement buttonSearch = $x("//div[@class='oxd-form-actions']/button[@type='submit']");
    SelenideElement buttonEmployeeList = $x("//li/a[text()='Employee List']");
    SelenideElement buttonConfirmDelete = $x("//div[@class='orangehrm-modal-footer']/button/i");
    SelenideElement buttonReports = $x("//li/a[text()='Reports']");
    SelenideElement selectDisplayFieldGroup = $x("//label[text()='Select Display Field Group']/parent::div/following-sibling::div//div[@class='oxd-select-text-input']");
    SelenideElement selectDisplayField = $x("//label[text()='Select Display Field']/parent::div/following-sibling::div//div[@class='oxd-select-text-input']");
    SelenideElement buttonAddDisplayField = $x("//label[text()='Select Display Field']/parent::div/following-sibling::div/parent::div/following-sibling::div//button/i");
    ElementsCollection fieldRequired = $$x("//span[text()='Required']");
    SelenideElement fieldRequiredFirstName = $x("//input[@name='firstName']/parent::div/following-sibling::span");
    SelenideElement fieldRequiredLastName = $x("//input[@name='lastName']/parent::div/following-sibling::span");
    SelenideElement fieldRequiredReportName = $x("//label[text()='Report Name']/parent::div/following-sibling::span");
    String inputValueXpathTemplate = "//div[@class='oxd-table-body']//input[@value='%d']/following-sibling::span/i";
    String nameReportXpathTemplate = "//h6[text()='%s']";

    @Step("Нажать кнопку Add")
    public PIMPage clickButtonAdd() {
        buttonAdd.click();
        return this;
    }

    @Step("Ввести Имя {firstName} Сотрудника")
    public PIMPage setFirstName(String firstName) {
        inputFirstName.setValue(firstName);
        return this;
    }

    @Step("Ввести Имя {employeeName} Сотрудника в поле Employee Information")
    public PIMPage setEmployeeName(String employeeName) {
        inputEmployeeName.setValue(employeeName);
        return this;
    }

    @Step("Ввести Фамилию {lastName} Сотрудника")
    public PIMPage setLastName(String lastName) {
        inputLastName.setValue(lastName);
        return this;
    }

    @Step("Ввести Id {id} Сотрудника")
    public PIMPage setId(int id) {
        inputId.setValue(String.valueOf(id));
        return this;
    }

    @Step("Очистить поле Id Сотрудника")
    public PIMPage clearId() {
        inputId.sendKeys(Keys.CONTROL + "A");
        inputId.sendKeys(Keys.BACK_SPACE);
        return this;
    }

    @Step("Нажать на кнопку Сохранить")
    public PIMPage clickButtonSave() {
        buttonSave.click();
        return this;
    }

    @Step("Проверить что сотрудник добавлен")
    public PIMPage checkEmployeeHasAdded() {
        cardPersonalDetails.shouldBe(visible);
        return this;
    }

    @Step("Выбрать всех сотрудников")
    public PIMPage checkboxAllEmployee() {
        selectAllEmployee.click();
        return this;
    }

    @Step("Выбрать сотрудника номер {number}")
    public PIMPage selectEmployee(int number) {
        $x(String.format(inputValueXpathTemplate, number - 1)).click();
        return this;
    }

    @Step("Выбрать первый чек-бокс из списка")
    public PIMPage selectFirsCheckbox() {
        $x(String.format(inputValueXpathTemplate, 0)).click();
        return this;
    }

    @Step("Проверить что сотрудник выбран")
    public PIMPage checkEmployeeSelected() {
        buttonDeleteEmployee.shouldBe(visible);
        return this;
    }

    @Step("Нажать на кнопку Поиск")
    public PIMPage clickButtonSearch() {
        buttonSearch.click();
        return this;
    }

    @Step("Нажать кнопку EmployeeList")
    public PIMPage clickButtonEmployeeList() {
        buttonEmployeeList.click();
        return this;
    }

    @Step("Нажать кнопку DeleteEmployee")
    public PIMPage clickButtonDelete() {
        buttonDeleteEmployee.click();
        return this;
    }

    @Step("Нажать кнопку подтверждения удаления")
    public PIMPage clickDeleteConfirmButton() {
        buttonConfirmDelete.click();
        return this;
    }

    @Step("Нажать кнопку Reports")
    public PIMPage clickButtonReports() {
        buttonReports.click();
        return this;
    }

    @Step("Ввести название {nameReport} для нового отчета ")
    public PIMPage setReportName(String nameReport) {
        inputReportName.setValue(nameReport);
        return this;
    }

    @Step("Выбрать значение поле DisplayFieldGroup")
    public PIMPage selectDisplayFieldGroup() {
        selectDisplayFieldGroup.click();
        $x("//span[text()='Personal']").click();
        return this;
    }

    @Step("Выбрать значение поле DisplayFieldGroup")
    public PIMPage selectDisplayField() {
        selectDisplayField.click();
        $x("//span[text()='Employee Id']").click();
        return this;
    }

    @Step("Добавить поле к отчету")
    public PIMPage clickAddDisplayFields() {
        buttonAddDisplayField.click();
        return this;
    }

    @Step("Проверка что новый отчет {nameReport} добавлен")
    public PIMPage checkAddedReport(String nameReport) {
        $x(String.format(nameReportXpathTemplate, nameReport)).shouldBe(visible);
        return this;
    }

    @Step("Ввести название отчета для раздела поиска")
    public PIMPage setReportNameForSearch(String nameReport) {
        inputReportName.setValue(nameReport);
        $x(String.format("//span[text()='%s']", nameReport)).click();
        return this;
    }

    @Step("Проверить обязательных полей при добавлении сотрудника")
    public PIMPage checkRequiredFieldEmployee() {
        fieldRequiredFirstName.shouldBe(visible);
        fieldRequiredLastName.shouldBe(visible);
        return this;
    }

    @Step("Проверить обязательных полей при добавлении отчета")
    public PIMPage checkRequiredFieldReport() {
        fieldRequiredReportName.shouldBe(visible);
        return this;
    }
}
