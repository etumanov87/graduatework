package ru.rvision.graduate.ui_tests;

import lombok.Getter;

@Getter
public enum NameSectionMainMenu {

    ADMIN("Admin"),
    PIM("PIM"),
    LEAVE("Leave"),
    TIME("Time"),
    RECRUITMENT("Recruitment"),
    MY_INFO("My Info"),
    PERFORMANCE("Performance"),
    DASHBOARD("Dashboard"),
    DIRECTORY("Directory"),
    MAINTENANCE("Maintenance"),
    CLAIM("Claim"),
    BUZZ("Buzz");

    final String title;
    NameSectionMainMenu(String title) {
        this.title = title;
    }
}
