package ru.rvision.graduate.ui_tests;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.rvision.graduate.ui_tests.pages.PageObjectSupplierLitres;


public class OrangeHRMTest implements PageObjectSupplierLitres {


    @BeforeSuite(description = "Авторизация на сайте")
    public void setUpAuth() {
        loginPage()
                .open()
                .setUserName(System.getProperty("UI_LOGIN"))
                .setUserPassword(System.getProperty("UI_PASSWORD"))
                .clickLoginButton();
    }

    @Test(description = "Проверить, что легенды 'Unassigned' существует и выключить ее", enabled = false)
    public void checkLegendKeyUnassignedDisable() {
        dashboardPage()
                .checkLegendKeyUnassigned()
                .findAndClickAllLegendKeyUnassigned(1)
                .checkAllNameLegendKeyUnassignedThrough();
    }

    @Test(description = "Добавить нового сотрудника")
    public void addNewEmployee() {
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.PIM.getTitle());
        pimPage()
                .clickButtonAdd()
                .setFirstName(faker.name().firstName())
                .setLastName(faker.name().lastName())
                .clearId()
                .setId(random.nextInt(1000, 1999))
                .clickButtonSave()
                .checkEmployeeHasAdded();
    }

    @DataProvider(name = "listNumberEmployee")
    public Object[][] dataProviderForCheckSelectedSpecificEmployee() {
        return new Object[][]{
                {1},
                {3},
                {5},
                {8}
        };
    }

    @Test(description = "Проверить что сотрудник под номером {number] выбран",
            dataProvider = "listNumberEmployee")
    public void checkSelectedSpecificEmployee(int number) {
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.PIM.getTitle());
        pimPage()
                .selectEmployee(number)
                .checkEmployeeSelected();
    }

    @DataProvider(name = "listIdEmployee")
    public Object[][] dataProviderForCheckSearchEmployeeById() {
        return new Object[][]{
                {random.nextInt(101, 999)},
                {random.nextInt(1100, 9999)},
                {random.nextInt(10000, 99999)},
        };
    }

    @Test(description = "Проверка поиска сотрудника по Id",
            dataProvider = "listIdEmployee")
    public void checkSearchEmployeeById(int id) {
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.PIM.getTitle());
        pimPage()
                .clickButtonAdd()
                .setFirstName(faker.name().firstName())
                .setLastName(faker.name().lastName())
                .clearId()
                .setId(id)
                .clickButtonSave()
                .checkEmployeeHasAdded()
                .clickButtonEmployeeList()
                .setId(id)
                .clickButtonSearch()
                .selectEmployee(1)
                .checkEmployeeSelected();
    }

    @DataProvider(name = "listNameEmployee")
    public Object[][] dataProviderForCheckSearchEmployeeByName() {
        return new Object[][]{
                {"Peter"},
                {"Natasha"},
                {faker.name().firstName()},
        };
    }

    @Test(description = "Проверка поиска сотрудника по Имени",
            dataProvider = "listNameEmployee")
    public void checkSearchEmployeeByName(String name) {
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.PIM.getTitle());
        pimPage()
                .clickButtonAdd()
                .setFirstName(name)
                .setLastName(faker.name().lastName())
                .clickButtonSave()
                .checkEmployeeHasAdded()
                .clickButtonEmployeeList()
                .setEmployeeName(name)
                .clickButtonSearch()
                .selectEmployee(1)
                .checkEmployeeSelected();
    }

    @Test(description = "Удалить сотрудника из списка")
    public void deleteEmployeeByName() {
        String name = faker.name().firstName();
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.PIM.getTitle());
        pimPage()
                .clickButtonAdd()
                .setFirstName(name)
                .setLastName(faker.name().lastName())
                .clickButtonSave()
                .checkEmployeeHasAdded()
                .clickButtonEmployeeList()
                .setEmployeeName(name)
                .clickButtonSearch()
                .selectEmployee(1)
                .clickButtonDelete()
                .clickDeleteConfirmButton();
    }

    @Test(description = "Добавление отчета с один полем")
    public void addNewReport() {
        String nameReport = faker.job().title();
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.PIM.getTitle());
        pimPage()
                .clickButtonReports()
                .clickButtonAdd()
                .setReportName(nameReport)
                .selectDisplayFieldGroup()
                .selectDisplayField()
                .clickAddDisplayFields()
                .clickButtonSave()
                .checkAddedReport(nameReport);
    }

    @Test(description = "Удаление отчета")
    public void deleteReport() {
        String nameReport = faker.job().title();
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.PIM.getTitle());
        pimPage()
                .clickButtonReports()
                .clickButtonAdd()
                .setReportName(nameReport)
                .selectDisplayFieldGroup()
                .selectDisplayField()
                .clickAddDisplayFields()
                .clickButtonSave()
                .checkAddedReport(nameReport)
                .clickButtonReports()
                .setReportNameForSearch(nameReport)
                .clickButtonSearch()
                .selectFirsCheckbox()
                .clickButtonDelete()
                .clickDeleteConfirmButton();
    }

    @Test(description = "Добавление нового кандидата")
    public void addNewCandidate() {
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.RECRUITMENT.getTitle());
        recruitmentPage()
                .clickButtonAdd()
                .setFirstName(faker.name().firstName())
                .setLastName(faker.name().lastName())
                .setEmail("test@test.com")
                .clickButtonSave()
                .checkCandidateHasAdded();
    }

    @Test(description = "Добавление новой вакансии")
    public void addNewVacancies() {
        String vacancyName = faker.job().position();
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.RECRUITMENT.getTitle());
        recruitmentPage()
                .clickButtonVacancies()
                .clickButtonAdd()
                .setVacancyName(vacancyName)
                .selectJobTitle()
                .setHiringManager()
                .clickButtonSave()
                .clickButtonCancel()
                .checkVacanciesHasAdded(vacancyName);
    }

    @Test(description = "Удаление первого кандидата из списка")
    public void deleteFirstCandidate() {
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.RECRUITMENT.getTitle());
        recruitmentPage()
                .selectCheckboxFirst()
                .clickButtonDelete();
    }

    @Test(description = "Удаление первой вакансии из списка")
    public void deleteFirstVacancies() {
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.RECRUITMENT.getTitle());
        recruitmentPage()
                .clickButtonVacancies()
                .selectCheckboxFirst()
                .clickButtonDelete();
    }

    @Test(description = "Проверить что при выборе пользователя появляется право окно с информацией")
    public void checkAppearanceRightSidebar() {
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.DIRECTORY.getTitle());
        directoryPage()
                .clickForFirstUser()
                .checkInfoForUser();
    }

    @Test(description = "Добавить Claim и согласовать его")
    public void addAndSubmitNewClaim() {
        String firstName = faker.name().firstName();
        String lastName = faker.name().lastName();
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.PIM.getTitle());
        pimPage()
                .clickButtonAdd()
                .setFirstName(firstName)
                .setLastName(lastName)
                .clearId()
                .setId(random.nextInt(1000, 1999))
                .clickButtonSave()
                .checkEmployeeHasAdded();
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.CLAIM.getTitle());
        claimPage()
                .clickButtonAssignClaim()
                .setEmployeeName(firstName, lastName)
                .selectEvent()
                .selectCurrency()
                .clickButtonCreate()
                .clickButtonSubmit()
                .clickButtonEmployeeClaims()
                .setEmployeeName(firstName, lastName)
                .clickButtonSearch()
                .checkClaimHasAdded();
    }

    @DataProvider(name = "listNameEvents")
    public Object[][] dataProviderForAddNewEvent() {
        return new Object[][]{
                {faker.address().state()},
                {faker.animal().name()},
                {faker.medical().symptoms()},
        };
    }

    @Test(description = "Добавить новое событие", dataProvider = "listNameEvents")
    public void addNewEvent(String nameEvent) {
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.CLAIM.getTitle());
        claimPage()
                .clickSectionEvent()
                .clickButtonAdd()
                .setNameEvent(nameEvent)
                .clickButtonSave()
                .checkEventHasAdded();
    }

    @Test(description = "Удалить событие")
    public void deleteEvent() {
        String nameEvent = faker.esports().event();
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.CLAIM.getTitle());
        claimPage()
                .clickSectionEvent()
                .clickButtonAdd()
                .setNameEvent(nameEvent)
                .clickButtonSave()
                .checkEventHasAdded()
                .setNameEventForSearch(nameEvent)
                .clickButtonSearch()
                .selectFirstCheckbox()
                .clickButtonDelete()
                .clickConfirmDelete();
    }

    @Test(description = "Проверка обязательных полей при добавлении сотрудника")
    public void checkRequiredFieldsByAddEmployee() {
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.PIM.getTitle());
        pimPage()
                .clickButtonAdd()
                .clickButtonSave()
                .checkRequiredFieldEmployee();
    }

    @Test(description = "Проверка обязательных полей при добавлении отчета")
    public void checkRequiredFieldsByAddReport() {
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.PIM.getTitle());
        pimPage()
                .clickButtonReports()
                .clickButtonAdd()
                .clickButtonSave()
                .checkRequiredFieldReport();
    }

    @Test(description = "Проверка обязательных полей при добавлении кандидата")
    public void checkRequiredFieldsByAddCandidate() {
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.RECRUITMENT.getTitle());
        recruitmentPage()
                .clickButtonAdd()
                .clickButtonSave()
                .checkRequiredFieldCandidate();
    }

    @Test(description = "Проверка обязательных полей при добавлении вакансии")
    public void checkRequiredFieldsByAddVacancies() {
        dashboardPage()
                .selectSectionMainMenu(NameSectionMainMenu.RECRUITMENT.getTitle());
        recruitmentPage()
                .clickButtonVacancies()
                .clickButtonAdd()
                .clickButtonSave()
                .checkRequiredFieldVacancies();
    }
}
